#!/usr/bin/env bash

sudo apt-get install -y git-core curl

curl https://raw.githubusercontent.com/creationix/nvm/v0.23.3/install.sh | bash

echo "source /home/vagrant/.nvm/nvm.sh" >> /home/vagrant/.profile
source /home/vagrant/.profile

nvm install 6
nvm alias default 6

# install g++ to compile stuff
#sudo apt-get install -y python-software-properties python g++ make git curl

# Adicionando o PPA do NodeJS
#curl -sL https://deb.nodesource.com/setup | sudo bash -

# Atualizando os repositórios
#sudo apt-get update

# Instalando o NodeJS
#sudo apt-get install -y nodejs