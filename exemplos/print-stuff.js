"use strict";

// Exibe uma string no console.
console.log("Hello World!!!");

// Exibe o valor da váriavel no console
let message = "Hello World!!!";
console.log(message);
// Concatenação de strings (Old School)
console.log(message + " Motherfucker");
// Interpolação de váriaveis (ES 2015)
console.log(`${message} Motherfucker`);

// Variáveis
// Váriaveis declaradas com var não respeitam o escopo onde foram declaradas 
(function exemploVar() {
	var v1 = 1;
	if (v1 >= 0) {
		var v2 = 2;
		console.log(`Var Interno: ${v1}`);
		console.log(`Var Interno: ${v2}`);
	}
	console.log(`Var Externo: ${v1}`);
	console.log(`Var Externo: ${v2}`);
})();

// Váriaveis declaradas com let ou const respeitam o escopo onde foram declaradas
(function exemploVar() {
	let l1 = 1;
	if (l1 >= 0) {
		let l2 = 2;
		console.log(`Let Interno: ${l1}`);
		console.log(`Let Interno: ${l2}`);
	}
	console.log(`Let Externo: ${l1}`);
	try {
		console.log(`Let Externo: ${l2}`);
	} catch(e) {
		console.error(e);
	}
	
})();

// Biblioteca padrão Node
// Os módulos permitem incluir código javascript externo em outras aplicações, ajudando na modularização de um sistema
// Essa inclusão ocorre por meio da instrução require, função global presente na api padrão do NodeJS
// Todas as funcionalidades providas pelo NodeJS estão definidas em módulos nativos, separando efetivamente a linguagem Javascript da biblioteca padrão do Node.
