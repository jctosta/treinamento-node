"use strict";

let jetpack = require('fs-jetpack');

console.log(jetpack.cwd());

if (jetpack.cwd() === "/vagrant") {
	let exemplos = jetpack.cwd('exemplos');
	console.log(exemplos.cwd());
	// var mussum = exemplos.read('file.txt');
	// console.log(mussum);
	
	exemplos.readAsync('file.txt').then(function(data) {
		console.log(data);
	});
	console.log("Teste");
}

function executaAlgumaCoisa(nome, callback) {
	callback(nome);
}

var f1 = function(data) {
	console.log(data);
};

executaAlgumaCoisa("Tosta", f1);

