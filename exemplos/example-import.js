"use strict";

// Realiza o import do módulo definido no arquivo example-module.js
let exampleModule = require('./example-module');
// Executa uma função do módulo
console.log(exampleModule.sayHello("Tosta"));
// Executa a outra função
let result = exampleModule.sumNumber(10, 10);
console.log(`Resultado da operação de soma: ${result}`);