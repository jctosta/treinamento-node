"use strict";

// O process é um módulo global e pode ser acessado de qualquer lugar sem necessitar de um import
console.log(`Diretório de Trabalho: ${process.cwd()}`);
console.log(`Sistema Operacional: ${process.platform}`);

// O módulo OS precisa ser importado antes de ser utilizado, para isso utilize a instrução require
// Módulos built-in e módulos no diretório node_modules não precisam ser importados pelo path, pode-se utilizar o nome do módulo, como definido no arquivo package.json
let os = require('os');
console.log(`Arquitetura do Processador: ${os.arch()}`);
console.log(`Diretório Home do Usuário: ${os.homedir()}`);
console.log(`Hostname: ${os.hostname()}`);
console.log(`Versão do Sistema Operacional ou Kernel: ${os.release()}`);