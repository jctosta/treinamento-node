"use strict";

// É possível utilizar módulos de terceiros nos programas em Node, para isso, utilize o comando npm install <nome_do_modulo> para baixar os arquivos necessários para o diretório node_modules. O flag --save pode ser utilizado para atualizar o arquivo package.json com as informações de depedências do projeto.
// Este exemplo importa a biblioteca Lodash, utilitário para manipulação de strings e  arrays no código atual.
let _ = require('lodash');

let os = require('os');

let nome = "João Carlos Tosta dos Santos";
let palavras = _.words(nome);
console.log(palavras);

console.log(_.camelCase(nome));
console.log(_.kebabCase(nome));
console.log(_.snakeCase(nome));