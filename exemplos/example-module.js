"use strict";

function sum(n1, n2) {
	return n1 + n2;
}

module.exports = {
	sayHello: function(name) {
		return `Hello ${name}`;
	},
	sumNumber: sum
};