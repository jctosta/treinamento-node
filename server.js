"use strict";

let express = require('express');
let app = express();

app.set('view engine', 'pug');

app.get('/api', function(req, res) {
	//res.send("Hello");
	res.json({
		message: "Hello"
	});
});

app.get('/', function (req, res) {
	res.render('index', { title: 'Hey', message: 'Hello there!'});
});

app.listen(3000, function() {
	console.log("Escutando na porta 3000.");
});