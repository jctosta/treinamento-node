# Treinamento Node JS

## Configuração

Execute o comando `npm install` no diretório raiz do projeto para instalar as depedências necessárias.

## Executando

- Utilize o comando `node <arquivo>` nos arquivos presentes no diretório __exemplos__.
- Utilize o comando `npm start` para iniciar o servidor __Express__ na porta 3000.
	- Abra um navegador e digite <http://localhost:3000/> para visualizar a página index renderizada.
	- Abra um navegador e digite <http://localhost:3000/api> para visualizar um webservice rest construído no framework Express.

## Próximos Passos

@TODO: Completar o readme.
